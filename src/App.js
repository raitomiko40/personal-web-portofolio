import React from 'react';
import Header from './components/header/Header';
import Home from './components/home/Home';
import About from './components/about/About';
import Skils from './components/skils/Skils';
import Education from './components/education/Education';
import Work from './components/work/Work';
import Contact from './components/contact/Contact';
import Footer from './components/footer/Footer';


const App = ()=> {
  return (
    <>

    <main className='bg-site bg-no-repeat bg-cover overflow-hidden'>
    <Header />
    <Home />
    <About />
    <Skils />
    <Education />
    <Work />
    <Contact />
    <Footer />
    </main>
    </>
  );
}

export default App;
