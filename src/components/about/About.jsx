import React from 'react';
import {FaArrowDown} from 'react-icons/fa'
//countup
import CountUp from 'react-countup';
//intersection observer
import {useInView} from 'react-intersection-observer'


const About = () => {
  const[ref, inView] = useInView({
    threshold: 0.5,
  })
  return (
    <section 
            className='section section-about' 
            id='about'
            ref={ref}
            >
            <div className='container mx-auto '>
              <div className='flex flex-col  lg:flex-row lg:items-center lg:gap-x-20  h-screen '>
                <div                
                  className='flex-1 bg-about bg-contain bg-no-repeat lg:h-[640px]  mix-blend-lighten bg-top '>
                </div>
                <div className='flex-1 '>
                <h2 
                    className='h2 text-accent lg:text-[32px] text-[25px] '>
                    ABOUT ME<span></span>
                  </h2>
                  <h3 
                    className='lg:mb-4 mb-2 text-justify lg:text-[18px] text-[15px]'>
                    <span>
                    Saya lulusan Teknik Informatika S1 dari Universitas Muslim Indonesia,
                      dan saya berspesialisasi dalam pengembangan aplikasi Web menggunakan HTML, CSS dan JavaScript maupun penggunaan NodeJs di sisi server-side.
                      Saya telah mengembangkan aplikasi web, dan saya memiliki pemahaman yang kuat tentang pengalaman pengguna dan prinsip desain.                   
                      </span>
                  </h3>
                  {/* stats */}
                    <div className='flex gap-x-6 lg:gap-x-10 mb-12 items-center '>
                      <div>
                        <div className='lg:text-[40px] text-[25px] font-tertiary text-gradient lg:mb-2'>
                          {
                            inView ?
                            <CountUp start={0} end={2} duration={3} />
                            :null
                          } +
                        </div>
                        <div className='font-primary text-sm tracking-[2px]'>
                          Years of <br/>
                          Experience
                        </div>
                      </div>
                      <div>
                        <div className='lg:text-[40px] text-[25px] font-tertiary text-gradient lg:mb-2'>
                          {
                            inView ?
                            <CountUp start={0} end={20} duration={3} />
                            :null
                          } +
                        </div>
                        <div className='font-primary text-sm tracking-[2px]'>
                          Project <br/>
                          Completed
                        </div>
                      </div>
                    </div>
                  <div 
                      className='flex max-w-max gap-x-6 items-center mb-12 mx-auto lg:mx-0'>
                      <a
                          href='#skills'
                          className=' cursor-pointer btn btn-sm lg:btn-lg flex items-center space-x-2'                        
                        >
                          <FaArrowDown className='w-5 h-5' />
                          <span>More About What I Do</span> 
                        </a>
                    </div>
                </div>
              </div>
            </div>
        </section>
  )
         
};

export default About;
