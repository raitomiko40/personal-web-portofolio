import React from 'react'
import './work.css'
import Portofolio from './Portofolio';
import { FaGithub, FaGitlab, FaLinkedin } from 'react-icons/fa';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; 

const Work = () => {
  return (
    <section className='section' id='work'>
      <div className='container mx-auto'>
        <div className='flex flex-col lg:flex-row  '>
          <div
            className='flex-1 lg:mb-0 lg:mt-0 mt-10'
          >
            {/* text */}
            <div>
              <h2 className='h2 leading-[1] text-accent lg:text-[50px] text-[25px]'>
                My latest <br /> Work
              </h2>
              <p className='max-w-sm lg:mb-2 text-justify text-[15px] lg:text-[20px]  mb-5'>
                Berikut beberapa project yang telah saya kerjakan dan juga link repository projectnya.
              </p>
              <div className='flex max-w-max gap-x-6 items-center mb-5 mx-auto lg:mx-0'>
                <a  className=' cursor-pointer btn btn-sm lg:btn-lg items-center flex'
                                href='#contact'
                                smooth={true}
                        >
                            Contact me
                        </a>              
              </div>
              <div
                className='flex items-center text-[20px] gap-x-6 max-w-max mx-auto lg:mx-0 mb-5'
              >
                <a href='https://github.com/RaitoMiko' target='_blank' rel='noopener noreferrer'>
                <FaGithub />
              </a>
              <a href='https://gitlab.com/raitomiko40' target='_blank' rel='noopener noreferrer'>
                <FaGitlab />
              </a>
              <a href='https://www.linkedin.com/in/micho-ahmad-s-6a4686124?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BURP2A%2BZDRC6yWsE7czqrGA%3D%3D' target='_blank' rel='noopener noreferrer'>
                <FaLinkedin />
              </a>
                
              </div>
            </div>
            {/* image */}
          </div>
          <div
            className='flex-1 flex flex-col gap-y-10  '
          >
            {/* carousel */}
            <Portofolio />
          </div>
        </div>
      </div>
    </section>
  )
}

export default Work