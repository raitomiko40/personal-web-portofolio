import React from 'react'
import { Carousel } from 'react-responsive-carousel';
import Img1 from '../../assets/porto/portfolio-img1111.png';
import Img2 from '../../assets/porto/portfolio-img22.png';
import Img3 from '../../assets/porto/portfolio-img333.png';
import Img4 from '../../assets/porto/portfolio-img3333.png';


const Portofolio = () => {
 
  return (
    <div>
        <Carousel showThumbs={false} showStatus={false} >
              <div className='group relative overflow-hidden border-2 border-white/50 rounded-xl '>
                {/* overlay */}
                <div className='group-hover:bg-black/70 w-full h-full absolute z-40 transition-all duration-300'></div>
                {/* img */}
                <img
                  className='group-hover:scale-125 transition-all duration-500 object-cover w-full h-full'
                  src={Img4}
                  alt=''
                />
                {/* pretitle */}
                <div className='absolute bottom-full left-12 group-hover:bottom-24 transition-all duration-500 z-50'>
                  <span className='text-xl h2'> Rock Paper Scissors App </span>
                </div>
                {/* project title */}
                <div className='absolute -bottom-full left-12 group-hover:bottom-14 transition-all duration-700 z-50'>
                  <span className='text-gradient'><a href="https://gitlab.com/raitomiko40/rock-paper-scissors-app-restfull-api" target='_blank'>Source Code</a></span>
                  <span> | </span>
                  <span className='text-gradient'><a href="https://rock-paper-scissors-app-client.vercel.app/" target='_blank'>Try Demo</a></span>
                </div>
              </div>
              <div className='group relative overflow-hidden border-2 border-white/50 rounded-xl '>
                {/* overlay */}
                <div className='group-hover:bg-black/70 w-full h-full absolute z-40 transition-all duration-300'></div>
                {/* img */}
                <img
                  className='group-hover:scale-125 transition-all duration-500 object-cover w-full h-full'
                  src={Img1}
                  alt=''
                />
                {/* pretitle */}
                <div className='absolute bottom-full left-12 group-hover:bottom-24 transition-all duration-500 z-50'>
                  <span className='text-xl h2'> Personal Web  </span>
                </div>
                {/* project title */}
                <div className='absolute -bottom-full left-12 group-hover:bottom-14 transition-all duration-700 z-50'>
                  <span className='text-gradient'><a href="https://gitlab.com/raitomiko40/personal-web-portofolio" target='_blank'>Source Code</a></span>
                  <span> | </span>
                  <span className='text-gradient'><a href="https://michoahmads.vercel.app/" target='_blank'>Try Demo</a></span>
                </div>
              </div>
              <div className='group relative overflow-hidden border-2 border-white/50 rounded-xl'>
                {/* overlay */}
                <div className='group-hover:bg-black/70 w-full h-full absolute z-40 transition-all duration-300'></div>
                {/* img */}
                <img
                  className='group-hover:scale-125 transition-all duration-500 object-cover w-full h-full'
                  src={Img2}
                  alt=''
                />
                {/* pretitle */}
                <div className='absolute bottom-full left-12 group-hover:bottom-24 transition-all duration-500 z-50'>
                  <span className='text-xl h2'> complete Task User App  </span>
                </div>
                {/* project title */}
                <div className='absolute -bottom-full left-12 group-hover:bottom-14 transition-all duration-700 z-50'>
                  <span className='text-gradient'><a href="https://gitlab.com/raitomiko40/app-cek-daftar-tugas-fullstack" target='_blank'>Source Code</a></span>
                  <span> | </span>
                  <span className='text-gradient'><a href="https://mock-test-micho-ahmad-s-client-side.vercel.app/" target='_blank'>Try Demo</a></span>
                </div>
              </div>
              <div className='group relative overflow-hidden border-2 border-white/50 rounded-xl'>
                {/* overlay */}
                <div className='group-hover:bg-black/70 w-full h-full absolute z-40 transition-all duration-300'></div>
                {/* img */}
                <img
                  className='group-hover:scale-125 transition-all duration-500 object-cover w-full h-full'
                  src={Img3}
                  alt=''
                />
                {/* pretitle */}
                <div className='absolute bottom-full left-12 group-hover:bottom-24 transition-all duration-500 z-50'>
                  <span className='text-xl h2'> User Pages with Handling Image | PDF  </span>
                </div>
                {/* project title */}
                <div className='absolute -bottom-full left-12 group-hover:bottom-14 transition-all duration-700 z-50'>
                  <span className='text-gradient'><a href="https://gitlab.com/raitomiko40/app-cek-daftar-tugas-fullstack" target='_blank'>Source Code</a></span>
                  <span> | </span>
                  <span className='text-gradient'><a href="https://binarfsw31ch11-fe.vercel.app/" target='_blank'>Try Demo</a></span>
                </div>
              </div>
            </Carousel>
    </div>
  )
}

export default Portofolio