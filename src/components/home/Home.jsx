import React from 'react'
//Image
import Image from '../../assets/banner.svg'
import CV from '../../assets/resume Fix.pdf'
//icon
import {FaGithub, FaGitlab, FaLinkedin, FaArrowDown, FaFileDownload} from 'react-icons/fa'
//type animation
import {TypeAnimation} from 'react-type-animation'
//motion
//variant
import { fadeIn } from '../variants'

const Home = () => {
  return (
    <section className='section h-screen flex items-center mt-8 lg:mt-0 ' id='home'>
            <div className='container mx-auto'>
              <div className='flex flex-col gap-y-8 lg:flex-row lg:items-center lg:gap-x-12 mt-10'>
                <div className='flex-1 text-center font-secondary lg:text-left '>
                <div         
                  className='lg:hidden max-w-[220px] flex justify-center mx-auto'>
                  <img src={Image} alt='Banner' className='block mx-auto' />
                </div>
                  <h1 
                    className=' font-bold leading-[0.8] text-[50px] lg:text-[110px]'>
                    SAYA<span></span>
                  </h1>
                  <div 
                    className='mb-6 text-[25px] lg:text-[47px] font-secondary font-semibold uppercase leading-[1] '>
                    <span className='text-white mr-4 ' >seorang</span>
                    <TypeAnimation
                      sequence={[
                        'Web Developer',
                        1500,
                        'Frontend Developer',
                        1500,
                        'Backend Developer',
                        1500,
                      ]}
                      speed={50}
                      className='text-accent'
                      wrapper='span'
                      repeat={Infinity}
                    />
                  </div>
                    <p
                      className='mb-8 max-w-lg max-auto lg:mx-0 text-justify text-[16px]'  >
                     Saya seorang Web Developer yang menyukai film dan musik. Saya berdedikasi untuk tetap menjadi yang terdepan di bidang saya dan terus belajar.                      
                    </p>
                    <div 
                      className='flex max-w-max gap-x-6 items-center mb-10 mx-auto lg:mx-0'>
                        <div className="contact__card">                        
                        <a
                          href='#about'
                          className=' cursor-pointer btn btn-sm lg:btn-lg flex items-center lg:space-x-2 '                        
                        >
                          <FaArrowDown className='w-5 h-5' />
                          <span className=''>About Me</span> 
                        </a>
                        </div>
                        <div className="contact__card ">
                          <a
                            download="" 
                            href={CV}
                            className='flex items-center space-x-2 cursor-pointer'
                            target='_blank'
                            rel='noopener noreferrer'
                          >
                            <FaFileDownload className='w-5 h-5 ' />
                            <span className=' text-gradient lg:text-lg text-sm'>Download CV</span>
                          </a>
                        </div>
                    </div>
                    <div 
                      className='flex text-[20px] gap-x-6 max-w-max mx-auto my-5 lg:mx-0'>
                      <a href='https://github.com/RaitoMiko' target='_blank' rel='noopener noreferrer'>
                        <FaGithub />
                      </a>
                      <a href='https://gitlab.com/raitomiko40' target='_blank' rel='noopener noreferrer'>
                        <FaGitlab />
                      </a>
                      <a href='https://www.linkedin.com/in/micho-ahmad-s-6a4686124?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BURP2A%2BZDRC6yWsE7czqrGA%3D%3D' target='_blank' rel='noopener noreferrer'>
                        <FaLinkedin />
                      </a>
                    </div>
                </div>
                <div 
                  variants={fadeIn('down', 0.3)}
                  initial="hidden" whileInView={'show'}          
                  className='hidden lg:flex flex-1 max-w-[320px] lg:max-w-[482px]'>
                  <img src={Image} alt='Banner'/>
                </div>
              </div>
            </div>
          </section>
  )
}

export default Home