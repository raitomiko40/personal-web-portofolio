import React from 'react';
import './slider.css';
import nodejs from '../../assets/nodejs.png';
import react from '../../assets/reactjs.png';
import sql from '../../assets/sql.png';
import js from '../../assets/js.png'
import html from '../../assets/html.png'
import sequelize from '../../assets/sequelize.png'
import mongo from '../../assets/mongo.png'
import css from '../../assets/css.png'
import pg from '../../assets/pg.png'
import bootstrap from '../../assets/bootstrap.png'
import tailwind from '../../assets/tailwind.png'
import php from '../../assets/php.png'
import Marquee from 'react-fast-marquee';

const Slider = () => {
  return (
    <div className='container'>

      <div className='announcement'>
        <Marquee speed={60} gradient={false} direction="left">
          <div className="announcement-text ">
            <img className="slide-image w-30 h-24" src={nodejs} alt='slide' />
          </div>
          <div className="announcement-text ">
            <img className="slide-image h-24" src={react} alt='slide' />
          </div>
          <div className="announcement-text">
            <img className="slide-image h-24" src={sql} alt='slide' />
          </div>
          <div className="announcement-text ">
            <img className="slide-image h-24" src={js} alt='slide' />
          </div>
          <div className="announcement-text ">
            <img className="slide-image h-24 " src={html} alt='slide' />
          </div>
          <div className="announcement-text ">
            <img className="slide-image h-24 " src={php} alt='slide' />
          </div>
        </Marquee>
        
      </div>
      <div className='announcement'>
        <Marquee direction="right">
          <div className="announcement-text text6">
            <img className="slide-image w-45 lg:h-24 h-24" src={mongo} alt='slide' />
          </div>
          <div className="announcement-text text7">
            <img className="slide-image lg:h-24 h-24" src={pg} alt='slide' />
          </div>
          <div className="announcement-text text7">
            <img className="slide-image lg:h-24 h-24" src={sequelize} alt='slide' />
          </div>
          <div className="announcement-text text7">
            <img className="slide-image lg:h-24 h-24" src={css} alt='slide' />
          </div>
          <div className="announcement-text text7">
            <img className="slide-image lg:h-24 h-24 " src={bootstrap} alt='slide' />
          </div>
          <div className="announcement-text text7">
            <img className="slide-image lg:h-24 h-24 " src={tailwind} alt='slide' />
          </div>
        </Marquee>
      </div>
    </div>
  );
};

export default Slider;
