import React from 'react';
import CV from '../../assets/CV.pdf'
import { FaGithub, FaGitlab, FaLinkedin, FaArrowDown } from 'react-icons/fa';
import Slider from './Slider';

const Skills = () => {
  return (
    <section className='section' id='skills'>
      <div className='container mx-auto'>
        <div className='flex flex-col gap-y-8 items-center lg:gap-x-12 mt-10'>
          <div className='flex-1 text-center font-secondary lg:text-left'>
            <h2 className='h2 text-accent text-[40px] lg:text-[50px] text-center justify-center items-center mb-2 lg:mb-5 mt-16 lg:mt-0'>
              My Skills
            </h2>
            <p className='text-center mb-8 leading-[2]'>My technical level</p>
            <div className='lg:mb-0 mb-10'>
              <Slider />
            </div>
            <div className='flex text-[20px] gap-x-6 max-w-max mx-auto mb-8 lg:mb-10 lg:mt-10'>
              <a href='https://github.com/RaitoMiko' target='_blank' rel='noopener noreferrer'>
                <FaGithub />
              </a>
              <a href='https://gitlab.com/raitomiko40' target='_blank' rel='noopener noreferrer'>
                <FaGitlab />
              </a>
              <a href='https://www.linkedin.com/in/micho-ahmad-s-6a4686124?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BURP2A%2BZDRC6yWsE7czqrGA%3D%3D' target='_blank' rel='noopener noreferrer'>
                <FaLinkedin />
              </a>
            </div>
            <div className='flex text-[20px] gap-x-6 max-w-max mx-auto mb-12'>
                <a
                          href='#education'
                          className=' cursor-pointer btn btn-sm lg:btn-lg flex items-center space-x-2'                        
                        >
                          <FaArrowDown className='w-5 h-5' />
                          <span>Look at My Education</span> 
                        </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Skills;
