import React from 'react'
import './education.css'
import {FaGithub, FaGitlab, FaLinkedin, FaArrowDown} from 'react-icons/fa'

const Education = () => {
  return (
    <section className='section' id='education'>
            <div className='container mx-auto'>
              <div className='flex flex-col gap-y-8 items-center lg:gap-x-12 mt-16 lg:mb-0 mb-10'>
                {/* tetx & image */}
                <div
                  className='flex-1 mx1 lg:mb-0 text-center font-secondary'>
                  <h2 className='h2 text-accent lg:text-[50px] leading-[1] lg:mb-5 mb-2'>
                    Education
                  </h2>
                  <h3 className='text-[15px] lg:text-[20px] leading-none'>
                     My Personal Journey!
                  </h3>
                </div>
                <div className=''>
                  <div 
                    className='flex-1 '>
                    <div className="qualification__data ">
                      <div>
                        <h3 className="text-gradient text-[20px] lg:text-[25px]">Informatics Engineering</h3>
                        <span className="text-[20px]">
                          Universitas Muslim Indonesia
                        </span>

                        <div className="qualification__calender">
                          <i className="uil uil-calendar-alt mr-2"></i>
                        2015 - 2022
                        </div>
                      </div>
                      <div>
                        <span className="qualification__rounder"></span>
                        <span className="qualification__line"></span>
                      </div>
                    </div>
                    <div className="qualification__data">
                      <div></div>
                      <div>
                        <span className="qualification__rounder"></span>
                      </div>

                      <div>
                        <h3 className="text-gradient text-[20px] lg:text-[25px]">
                          Fullstack Web Developer
                        </h3>
                        <span className=" text-[20px] ">
                          Binar Academy
                        </span>

                        <div className="qualification__calender">
                          <i className="uil uil-calendar-alt mr-2"></i>
                        2022 - 2023
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div 
                      className='flex text-[20px] gap-x-6 max-w-max mx-auto lg:mx-0'>
                      <a href='https://github.com/RaitoMiko' target='_blank' rel='noopener noreferrer'>
                        <FaGithub />
                      </a>
                      <a href='https://gitlab.com/raitomiko40' target='_blank' rel='noopener noreferrer'>
                        <FaGitlab />
                      </a>
                      <a href='https://www.linkedin.com/in/micho-ahmad-s-6a4686124?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BURP2A%2BZDRC6yWsE7czqrGA%3D%3D' target='_blank' rel='noopener noreferrer'>
                        <FaLinkedin />
                      </a>
                    </div>
                    <div className='flex max-w-max gap-x-6 items-center mb-5 mx-auto lg:mx-0'>
                    <a
                          href='#work'
                          className=' cursor-pointer btn btn-sm lg:btn-lg flex items-center space-x-2'                        
                        >
                          <FaArrowDown className='w-5 h-5' />
                          <span>Look at My Work</span> 
                        </a>
                    </div>
              </div>
            </div>
          </section>
  )
}

export default Education