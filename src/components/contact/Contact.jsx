import React from 'react'
import {FaGithub, FaGitlab, FaLinkedin, FaWhatsapp} from 'react-icons/fa'

const Contact = () => {
  return (
    <section className='py-16 lg:section' id='contact'>
    <div className='container mx-auto'>
      <div className='flex flex-col'>
        {/* text */}
        <div className='flex justify-center items-center lg:mt-0 mt-16 mb-16'>
          <div>
            <h4 className='text-[40px] lg:text-[60px] uppercase text-accent font-medium mb-2 tracking-wide leading-[1] flex justify-center items-center lg:mt-0 mt-16'>
               Get in Touch 
            </h4>
            <h2 className='text-[15px] lg:text-[20px] leading-none mb-12 flex justify-center items-center'>
                For business inquiry please send email
            </h2>
            <div  className='flex text-[20px] gap-x-6 max-w-max mx-auto justify-center items-center mb-10 lg:mb-10'>
              <a href='https://github.com/RaitoMiko' target='_blank' rel='noopener noreferrer'>
                <FaGithub />
              </a>
              <a href='https://gitlab.com/raitomiko40' target='_blank' rel='noopener noreferrer'>
                <FaGitlab />
              </a>
              <a href='https://www.linkedin.com/in/micho-ahmad-s-6a4686124?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BURP2A%2BZDRC6yWsE7czqrGA%3D%3D' target='_blank' rel='noopener noreferrer'>
                <FaLinkedin />
              </a>
              
            </div>
            <div className='flex max-w-max gap-x-6 items-center lg:mb-12 mb-16 mx-auto justify-center items-center'>
              <div className="contact__card">                        
                <a
                  href='http://api.whatsapp.com/send?phone=+6285756942080&text=Hello, more information!'
                  className='btn  btn-sm lg:btn-lg flex items-center space-x-2'
                  target='_blank' // Added this line to open link in a new tab
                  rel='noopener noreferrer' // Added this for security reasons
                >
                  <FaWhatsapp className='w-5 h-5' />
                  <span>whatsapp</span>
                </a>
                </div>
                <div className="contact__card">      
                    <a href="mailto:mikobangra@gmail.com" className="btn btn-sm lg:btn-lg flex items-center justify-center space-x-2">
                    <i className="bx bx-mail-send contact__card-icon w-5 h-5 flex items-center justify-center "></i>
                <span> Email</span>
                </a>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  )
}

export default Contact